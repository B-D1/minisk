import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Cubic {
	public static double MAX = 1000000; // The maximum number
	public static double MIN = -1000000; // The minimum number

	public static Double binarySearch(double minv, double maxv, double a,
			double b, double c, double d) {
		// [l,r] is the interval for binary search
		double l = minv;
		double r = maxv;
		if (cubicCount(minv, a, b, c, d) * cubicCount(maxv, a, b, c, d) > 0) // No
																				// root
			return null;
		else {
			while (r - l > 0.000000000001) // When the difference of r and l is
											// small enough, stop the search
			{
				double m = (r + l) / 2;
				if (cubicCount(m, a, b, c, d) * cubicCount(l, a, b, c, d) > 0)
					l = m;
				else
					r = m;
			}
			return l;
		}
	}

	// This function gets all the roots of cubic equation a*x^3+b*x^2+c*x+d=0
	public static ArrayList<Double> soluteCubic(int a, int b, int c, int d) {

		double[] peak; // peak is the array of roots of the derivative of the
						// cubic equation
		ArrayList<Double> ans = new ArrayList<Double>(); // ans is the list of
															// all roots of the
															// cubic equation
		peak = getPeak(3 * a, 2 * b, c); // Get the roots of the derivative
											// 3*a*x^2+2*b*x+c

		if (peak == null)
			ans.add(binarySearch(MIN, MAX, a, b, c, d));
		else {
			if (cubicCount(peak[0], a, b, c, d) == 0) {
				ans.add(peak[0]);
				Double r = null;
				r = binarySearch(peak[1], MAX, a, b, c, r);
				if (r != null)
					ans.add(r);
			} else {
				Double r;
				r = binarySearch(MIN, peak[0], a, b, c, d);
				if (r != null)
					ans.add(r);
				if (cubicCount(peak[1], a, b, c, d) == 0)
					ans.add(peak[1]);
				else {
					r = binarySearch(peak[0], peak[1], a, b, c, d);
					if (r != null)
						ans.add(r);
					r = binarySearch(peak[1], MAX, a, b, c, d);
					if (r != null)
						ans.add(r);
				}
			}

		}
		return ans;

	}

	// This function count a*x^3+b*x^2+c*x+d for a specific input x
	public static double cubicCount(double x, double a, double b, double c,
			double d) {
		return ((a * x + b) * x + c) * x + d;
	}

	// If the quadratic equation has less than 2 roots, return null. Ohterwise,
	// return the two roots
	public static double[] getPeak(double a, double b, double c) {
		if (delta(a, b, c) <= 0)
			return null;
		else
			return quadraticSolute(a, b, c);

	}

	// Get the delta(b^2-4*a*c) for a quadratic equation
	public static double delta(double a, double b, double c) {
		return b * b - 4 * a * c;
	}

	// This function get all roots of a quadratic equation a*x^2+b*x+c=0
	public static double[] quadraticSolute(double a, double b, double c) {
		double[] ret = new double[2];
		ret[0] = (-b - sqrt(delta(a, b, c))) / 2 / a;
		ret[1] = (-b + sqrt(delta(a, b, c))) / 2 / a;
		if (a < 0) {
			double q = ret[0];
			ret[0] = ret[1];
			ret[1] = q;
		}
		return ret;
	}

	// This function count the square root of a number n
	public static double sqrt(double n) {
		double l;
		double r;
		l = 0;
		r = MAX;
		while (r - l > 0.000000000001) {
			double m = (l + r) / 2;
			if (m * m < n)
				l = m;
			else
				r = m;
		}
		return l;
	}

	public static void main(String[] args) {

		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int a;
		int b;
		int c;
		int d;
	
			while(true)
			{
			System.out.println("Please input a");
				try
				{
					a = Integer.parseInt(in.readLine());
					break;
				}
				catch (Exception e)
				{
					System.out.println("Input error");	
				}
			}
			while(true)
			{
			System.out.println("Please input b");
				try
				{
					b = Integer.parseInt(in.readLine());
					break;
				}
				catch (Exception e)
				{
					System.out.println("Input error");	
				}
			}
			while(true)
			{
			System.out.println("Please input c");
				try
				{
					c = Integer.parseInt(in.readLine());
					break;
				}
				catch (Exception e)
				{
					System.out.println("Input error");	
				}
			}
			while(true)
			{
			System.out.println("Please input d");
				try
				{
					d = Integer.parseInt(in.readLine());
					break;
				}
				catch (Exception e)
				{
					System.out.println("Input error");	
				}
			}
			int sum = (a < 0 ? -a : a) + (b < 0 ? -b : b) + (c < 0 ? -c : c)
					+ (d < 0 ? -d : d);
			if (sum < 0)
				sum = -sum;
			if (sum % 2 == 0 || sum < 2 || PrimeNumber.primeCheck(sum)) {
				System.out.println("sum of a,b,c and d is not odd composite");
			} else if (a == 0) {
				System.out.println("a should not be 0");
			} else {
				ArrayList ans = soluteCubic(a, b, c, d);
				for (int i = 0; i < ans.size(); i++)
					System.out.println(ans.get(i));
			}
	
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		

	}
}
