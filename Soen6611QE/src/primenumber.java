import java.util.*;

public class primenumber
{
	public static void main(String args[])
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter the number to check primality");
		double num = scan.nextDouble();
		primeCheck(num);
	}
		
	//Function to check on the prime or composite check
	public static boolean primeCheck(double num)
	{
		double i =0;
		boolean flag = false;
		if(num<=1){
			System.out.println("Entered number is not a prime number)");	
		}
		else
		{
			for (i = 2; i < num; i++) 
			{
				double z = num % i;
				if (z == 0)
				{
					System.out.println(num + " is not a prime");
					i = num;	
				}
			}
			if (i == num)
			{
				System.out.println("Given number is prime");
				flag = true;
			}
		}
	
	return flag;
	}
}
