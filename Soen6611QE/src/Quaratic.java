import java.util.Scanner;

import javax.swing.JOptionPane;

public class Quadratic extends primenumber
{
	public static void main(String[] args)
	{
		double a;
		Scanner scan = new Scanner(System.in);
		// To check for condition a not equal to 0
		do
		{
			System.out.println("Enter non zero operand a : ");
			a = scan.nextDouble();
		}while(a == 0);
		System.out.println(" Enter operand b : ");
		double b = scan.nextDouble();
		System.out.println(" Enter operand c :  ");
		double c = scan.nextDouble();
		//Method call to calculate absolute sum
		double p= sum(a,b,c);
		//Method call to check for prime number
		boolean check = primeCheck(p);
		if(check == true)
		{
			quadraticEq(a,b,c);
		}
		else 
		{
			System.out.println("Absolute sum of a,b and c should be a prime number");
		}
	}   

	private static void quadraticEq(double a,double b,double c)
     {
         // d is the discriminator
    	 double d = (b * b - 4 * a * c);
    	 if (d > 0)
    	 {
    		 	double number = sqrt(d);
    		 	double r1 = ( -b + number) / (2*a);
    		 	double r2 = ( -b - number) / (2*a);
    		 	double r3 = ((2*c)/(-b - number));
    		 	double r4 = ((2*c)/(-b + number));
    		 	if(c == 0)
    		 	{
    	              System.out.print("The equation has two real roots" +"\n"+" The roots are : "+  r1+" and " +r2);
    	        }
    		 	// if c not equal to 0
    		 	else
    		 	{
    		 		if(b >= 0)
    		 		{
    		 			System.out.print("The equation has two real roots" +"\n"+" the roots are : "+  r1+" and " +r4);
    		 		}
    		 		else
    		 		{
    		 			System.out.print("The equation has two real roots" +"\n"+" the roots are : "+  r2+" and " +r3); 
    		 		}
    		 	}
    	 }
     
    	 else if(d ==0) 
    	 {
          
             System.out.print("Both the roots have same value"+  "\n"+ " The root is :  " +(-b /2 * a));
    	 }
    	 // if d < 0
    	 else
    	 {
    		 d=-d;
    		 double m=(-b/(2*a));
    		 double number1=sqrt(d);
    		 double s=(number1/(2*a));

            System.out.print("the roots are imaginary"+ "\n"+ "The roots are :"+ m+"+i"+ s+"and"+ m+"-i"+ s);
        }
      
    }
	
	//Function to calculate square root
	private static double sqrt(double number) 
	{
		double t;
		double squareRoot = number / 2;
		do 
		{
			t = squareRoot;
			squareRoot = (t + (number / t)) / 2;	
		} while ((t - squareRoot) != 0);
		return squareRoot;
	}
	
	//Function to calculate absolute sum
	private static double sum(double a,double b,double c)
	{
		double sum;
		double abs_a = (a < 0) ? -a : a;
		double abs_b = (b < 0) ? -b : b;
		double abs_c = (c < 0) ? -c : c;
		sum= abs_a+ abs_b + abs_c;
		System.out.println("Absolute sum of a,b and c is " +sum);
		return sum;
	}
}